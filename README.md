# Ansible role for Redis installation

## Introduction

[Redis](http://www.redis.io/) is a in-memory data structure store,
running as a daemon.

This role installs and configure the server. There is no configuration
for now, since none was needed at the moment.
